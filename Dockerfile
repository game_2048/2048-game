ARG OS_VERSION=16.20.2-alpine3.18

FROM node:$OS_VERSION AS builder

WORKDIR /app

COPY package*.json ./

RUN npm install --include=dev

COPY . .

RUN npm run build

FROM node:$OS_VERSION

WORKDIR /app

COPY --from=builder /app ./

CMD ["npm", "start"]

HEALTHCHECK --interval=30s --timeout=10s --retries=3 \
  CMD wget -qO- http://172.17.0.2:8080 || exit 1

EXPOSE 8080